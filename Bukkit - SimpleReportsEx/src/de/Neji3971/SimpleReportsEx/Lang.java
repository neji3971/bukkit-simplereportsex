package de.Neji3971.SimpleReportsEx;

import java.io.File;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Lang{

	public String				lang;
	public File					lang_File;
	public FileConfiguration	language;

	public Lang(File lang_File){
		this.lang_File = lang_File;
		language = new YamlConfiguration();
		try{
			language.load(lang_File);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public String getMsg(String key){
		return Inc.parseCC(language.getString(key));
	}

	public void reloadLanguage(File lang_File){
		this.lang_File = lang_File;
		try{
			language.load(lang_File);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}