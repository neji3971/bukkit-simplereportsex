package de.Neji3971.SimpleReportsEx;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import org.bukkit.command.CommandSender;

public class GetRepThread implements Runnable{

	private int				ID;
	private CommandSender	sender;

	public GetRepThread(int ID, CommandSender sender){
		this.ID = ID;
		this.sender = sender;
	}

	@Override
	public void run(){
		getReport(ID, sender);
	}

	public static void getReport(int ID, CommandSender sender){
		String reason = null, uuid = null, world = null;
		long timestamp = 0;
		boolean b = true;
		if(ReportsMain.backend.equals("sql")){
			ResultSet res = ReportsMain.sql.executeQuery("SELECT report,reporter_id,timestamp,active,world FROM reports WHERE id = '" + ID + "' AND active = '1'");
			try{
				if(res.first()){
					reason = res.getString("report");
					ResultSet res2 = ReportsMain.sql.executeQuery("SELECT uuid FROM players WHERE id = '" + res.getInt("reporter_id") + "'");
					while(res2.next()){
						uuid = res2.getString("uuid");
					}
					timestamp = res.getLong("timestamp");
					world = res.getString("world");
				}else{
					sender.sendMessage(ReportsMain.lang.getMsg("no-report"));
					b = false;
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}else{
			if(ReportsMain.reports.get("reports." + ID) != null){
				reason = ReportsMain.reports.getString("reports." + ID + ".reason");
				uuid = ReportsMain.reports.getString("reports." + ID + ".issued-by");
				timestamp = ReportsMain.reports.getLong("reports." + ID + ".timestamp");
				world = ReportsMain.reports.getString("reports." + ID + ".world");
			}else{
				sender.sendMessage(ReportsMain.lang.getMsg("no-report"));
				b = false;
			}
		}
		if(b){
			String name = ReportsMain.getNameFromUUID(UUID.fromString(uuid));
			long millis = System.currentTimeMillis() - timestamp;
			long days, hours, minutes, seconds, buffer;
			buffer = millis % (24 * 60 * 60 * 1000);
			millis -= buffer;
			days = millis / (24 * 60 * 60 * 1000);
			millis += buffer - days * 24 * 60 * 60 * 1000;
			buffer = millis % (60 * 60 * 1000);
			millis -= buffer;
			hours = millis / (60 * 60 * 1000);
			millis += buffer - hours * 60 * 60 * 1000;
			buffer = millis % (60 * 1000);
			millis -= buffer;
			minutes = millis / (60 * 1000);
			millis += buffer - minutes * 60 * 1000;
			buffer = millis % (1000);
			millis -= buffer;
			seconds = millis / (1000);
			millis += buffer - seconds * 1000;
			sender.sendMessage(ReportsMain.lang.getMsg("report-detail"));
			sender.sendMessage(ReportsMain.lang.getMsg("report-reason").replace("[REASON]", reason));
			sender.sendMessage(ReportsMain.lang.getMsg("issued-by").replace("[ISSUED]", name));
			sender.sendMessage(ReportsMain.lang.getMsg("world").replace("[WORLD]", world));
			sender.sendMessage(ReportsMain.lang.getMsg("time").replace("[DAYS]", new Long(days).toString()).replace("[HOURS]", new Long(hours).toString())
					.replace("[MINUTES]", new Long(minutes).toString()).replace("[SECONDS]", new Long(seconds).toString()).replace("[MILLIS]", new Long(millis).toString()));
		}
	}
}