package de.Neji3971.SimpleReportsEx;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;

public class SQL_sre{

	Connection			conn;
	public final String	adress;
	public final String	user;
	public final String	pass;
	public final String	database;
	public final String	prefix;
	public final int	port;

	public SQL_sre(String adress, String user, String pass, int port, String database, String prefix){
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		this.adress = adress;
		this.user = user;
		this.pass = pass;
		this.port = port;
		this.database = database;
		this.prefix = prefix;
	}

	public void executeUpdate(String query){
		query = query.replace("reports", prefix + "_reports");
		query = query.replace("players", prefix + "_players");
		String url = "jdbc:mysql://" + adress + ":" + port + "/" + database;
		try{
			if(conn == null || !conn.isValid(1)){
				conn = DriverManager.getConnection(url, user, pass);
			}
			conn.createStatement().executeUpdate(query);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	public ResultSet executeQuery(String query){
		query = query.replace("reports", prefix + "_reports");
		query = query.replace("players", prefix + "_players");
		String url = "jdbc:mysql://" + adress + ":" + port + "/" + database;
		ResultSet result = null;
		try{
			if(conn == null || !conn.isValid(1)){
				conn = DriverManager.getConnection(url, user, pass);
			}
			PreparedStatement statement = conn.prepareStatement(query);
			result = statement.executeQuery();
		}catch(Exception e){
			ReportsMain.log(Level.SEVERE, "Connection to SQL Database not possible!");
			e.printStackTrace();
		}
		return result;
	}

	public int getIDOfPlayer(String uuid){
		int ID = 0;
		String player = ReportsMain.getNameFromUUID(UUID.fromString(uuid));
		ResultSet res = this.executeQuery("SELECT id FROM players WHERE uuid = '" + uuid + "'");
		while(true){
			try{
				if(res.next()){
					ID = res.getInt("id");
				}else{
					break;
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		if(ID == 0){
			this.executeUpdate("INSERT INTO `players` (`id`, `uuid`, `last known name`) VALUES (NULL, '" + uuid + "', '" + player + "')");
		}else{
			String player_old = "";
			res = this.executeQuery("SELECT `last known name` FROM players WHERE uuid = '" + uuid + "'");
			try{
				if(res.next()){
					player_old = res.getString("last known name");
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			if(!player_old.equals(player)){
				this.executeUpdate("UPDATE players SET `last known name` = '" + player + "' WHERE uuid = '" + uuid + "'");
			}
		}
		res = this.executeQuery("SELECT id FROM players WHERE uuid = '" + uuid + "'");
		ID = 0;
		while(true){
			try{
				if(res.next()){
					ID = res.getInt("id");
				}else{
					break;
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		return ID;
	}
}