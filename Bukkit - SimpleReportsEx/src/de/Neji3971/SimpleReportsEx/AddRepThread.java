package de.Neji3971.SimpleReportsEx;

import java.util.List;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddRepThread implements Runnable{

	private String			reason;
	private CommandSender	sender;

	public AddRepThread(String reason, CommandSender sender){
		this.reason = reason;
		this.sender = sender;
	}

	@Override
	public void run(){
		if(sender instanceof Player){
			addReport(reason, (Player) sender);
		}else{
			sender.sendMessage(ReportsMain.lang.getMsg("report-player-only"));
		}
	}

	public static void addReport(String reason, Player sender){
		int x, y, z;
		long timestamp = System.currentTimeMillis();
		Location loc = ((Player) sender).getLocation();
		x = loc.getBlockX();
		y = loc.getBlockY();
		z = loc.getBlockZ();
		if(ReportsMain.backend.equals("sql")){
			int pid = ReportsMain.sql.getIDOfPlayer(ReportsMain.getUUIDFromName(sender.getName()));
			ReportsMain.sql.executeUpdate("INSERT INTO `reports`(report,reporter_id,world,x,y,z,timestamp,active)VALUES('" + reason + "','" + pid + "','" + sender.getWorld().getName() + "','" + x
					+ "','" + y + "','" + z + "','" + timestamp + "','1')");
		}else{
			int no = 1;
			for(int i = 1;; i++){
				if(ReportsMain.reports.get("reports." + i) == null){
					no = i;
					break;
				}
			}
			ReportsMain.reports.set("reports." + no + ".reason", reason);
			ReportsMain.reports.set("reports." + no + ".world", sender.getWorld().getName());
			ReportsMain.reports.set("reports." + no + ".x", x);
			ReportsMain.reports.set("reports." + no + ".y", y);
			ReportsMain.reports.set("reports." + no + ".z", z);
			ReportsMain.reports.set("reports." + no + ".issued-by", ReportsMain.getUUIDFromName(sender.getName()));
			ReportsMain.reports.set("reports." + no + ".timestamp", System.currentTimeMillis());
			ReportsMain.reports.set("reports." + no + ".active", true);
			List<String> buffer = ReportsMain.reports.getStringList("keys");
			buffer.add(new Integer(no).toString());
			ReportsMain.reports.set("keys", buffer);
		}
		sender.sendMessage(ReportsMain.lang.getMsg("report-added"));
	}
}