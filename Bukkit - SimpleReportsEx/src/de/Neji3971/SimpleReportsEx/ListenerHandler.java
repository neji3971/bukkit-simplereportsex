package de.Neji3971.SimpleReportsEx;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class ListenerHandler implements Listener{

	private final ReportsMain plugin;
	
	public ListenerHandler(ReportsMain plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
			
			@Override
			public void run() {
				if(event.getPlayer().hasPermission("SimpleReportsEx.getUnsolvedReportsOnLogin") || event.getPlayer().isOp()){
					ReportsMain.getReports(event.getPlayer(), false);
				}
			}
			
		}, 100L);
	}
}