package de.Neji3971.SimpleReportsEx;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.bukkit.command.CommandSender;

public class Purger implements Runnable{

	private final CommandSender	sender;

	public Purger(CommandSender sender){
		this.sender = sender;
	}

	public void purgeWarnings(CommandSender sender){
		int deleted = 0;
		if(ReportsMain.backend.equalsIgnoreCase("file")){
			List<String> keys = ReportsMain.reports.getStringList("keys");
			for(int i = 0; i < keys.size(); i++){
				if(!ReportsMain.reports.getBoolean("reports." + keys.get(i) + ".active")){
					ReportsMain.delReport(Integer.parseInt(keys.get(i)));
					deleted++;
				}
			}
		}else{
			ResultSet res = ReportsMain.sql.executeQuery("SELECT COUNT(id) AS count_id FROM reports WHERE active = '0'");
			try{
				if(res.next()){
					deleted = res.getInt("count_id");
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			ReportsMain.sql.executeUpdate("DELETE FROM reports WHERE active = '0'");
		}
		sender.sendMessage(ReportsMain.lang.getMsg("purge-confirm").replace("[COUNT]", new Integer(deleted).toString()));
	}

	@Override
	public void run(){
		purgeWarnings(sender);
	}

}
