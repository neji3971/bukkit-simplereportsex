package de.Neji3971.SimpleReportsEx;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.java.JavaPlugin;
import com.evilmidget38.NameFetcher.NameFetcher;
import com.evilmidget38.UUIDFetcher.UUIDFetcher;

public class ReportsMain extends JavaPlugin{

	File							configFile			= new File(getDataFolder(), "config.yml");	;
	File							reportFile			= new File(getDataFolder(), "reports.yml");
	File							lang_EN;
	File							lang_DE;
	static Lang						lang;
	public static FileConfiguration	config;
	public static FileConfiguration	reports;
	static SQL_sre					sql;
	private static final Logger		logger				= Logger.getLogger("minecraft");
	static String					backend;
	static boolean					awaitingResponse	= false;

	@Override
	public void onEnable(){
		Timer warningssaver = new Timer();
		warningssaver.schedule(new TimerTask(){
			@Override
			public void run(){
				saveYamls();
			}
		}, 300000, 300000);
		lang_EN = new File(getDataFolder(), "lang" + File.separator + "English.yml");
		lang_DE = new File(getDataFolder(), "lang" + File.separator + "German.yml");
		YamlConfiguration temp = new YamlConfiguration();
		try{
			temp.load(lang_EN);
			if(temp.get("version") == null || !temp.getString("version").equals("1.1")){
				Inc.copy(getResource("English.yml"), lang_EN);
				log("Recreating English language-file (newer version)");
			}
			temp.load(lang_DE);
			if(temp.get("version") == null || !temp.getString("version").equals("1.1")){
				Inc.copy(getResource("German.yml"), lang_DE);
				log("Recreating German language-file (newer version)");
			}
		}catch(FileNotFoundException e2){
		}catch(IOException e2){
		}catch(InvalidConfigurationException e2){
		}
		try{
			firstRun();
		}catch(Exception y){
			y.printStackTrace();
		}
		config = new YamlConfiguration();
		try{
			config.load(configFile);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}catch(InvalidConfigurationException e){
			e.printStackTrace();
		}
		config.addDefault("language", "English");
		config.addDefault("backend", "file");
		config.addDefault("sql.server", "127.0.0.1");
		config.addDefault("sql.port", 3306);
		config.addDefault("sql.database", "simplereports");
		config.addDefault("sql.user", "sre");
		config.addDefault("sql.password", "sre");
		config.addDefault("sql.table-prefix", "sre");
		try{
			config.save(configFile);
		}catch(IOException e){
			e.printStackTrace();
		}
		backend = config.getString("backend");
		if(backend.equalsIgnoreCase("file")){
			reports = new YamlConfiguration();
		}else if(backend.equalsIgnoreCase("sql")){
			sql = new SQL_sre(config.getString("sql.server"), config.getString("sql.user"), config.getString("sql.password"), config.getInt("sql.port"), config.getString("sql.database"),
					config.getString("sql.table-prefix"));
			sql.executeUpdate("CREATE TABLE IF NOT EXISTS `players` (`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto-generated ID of the player', `uuid` varchar(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'The UUID of the player', `last known name` varchar(32) NOT NULL COMMENT 'The last username that the player with this UUID used', PRIMARY KEY (id))");
			sql.executeUpdate("CREATE TABLE IF NOT EXISTS `reports` (`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto-generated ID of that report', `report` varchar(128) NOT NULL COMMENT 'The report-text', `reporter_id` int(11) NOT NULL COMMENT 'The player that created this report', `world` varchar(128) NOT NULL COMMENT 'The world this report was created in', `x` int(11) NOT NULL COMMENT 'The x-coordinate where the report was created', `y` int(11) NOT NULL COMMENT 'The y-coordinate where the report was created', `z` int(11) NOT NULL COMMENT 'The z-coordinate where the report was created', `timestamp` bigint(20) NOT NULL COMMENT 'The time when the report was created', `active` tinyint(1) NOT NULL COMMENT 'If the report is active or not', PRIMARY KEY (id))");
		}else{
			log(Level.SEVERE, "No valid input in config for backend! Using file");
			backend = "file";
		}
		loadYamls();
		lang = new Lang(new File(getDataFolder(), "lang" + File.separator + config.getString("language") + ".yml"));
		getServer().getPluginManager().registerEvents(new ListenerHandler(this), this);
		if(!Bukkit.getOnlineMode()){
			log(Level.WARNING,
					"This Plugin cannot work properly on a server running in offline-mode unless you have a bungeecord-server and the Proxy is running in online-mode, then it is totally fine!");
		}
	}

	@Override
	public void onDisable(){
		saveYamls();
	}

	private void firstRun() throws Exception{
		if(!configFile.exists()){
			configFile.getParentFile().mkdirs();
			Inc.copy(getResource("config.yml"), configFile);
		}
		if(!reportFile.exists()){
			reportFile.getParentFile().mkdirs();
			Inc.copy(getResource("reports.yml"), reportFile);
		}
		if(!lang_EN.exists()){
			lang_EN.getParentFile().mkdirs();
			Inc.copy(getResource("English.yml"), lang_EN);
		}
		if(!lang_DE.exists()){
			lang_DE.getParentFile().mkdirs();
			Inc.copy(getResource("German.yml"), lang_DE);
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(cmd.getName().equalsIgnoreCase("SimpleReportsEx") || cmd.getName().equalsIgnoreCase("sre") || cmd.getName().equalsIgnoreCase("simplereports")){
			if(args.length > 1){
				sender.sendMessage(lang.getMsg("many-args"));
			}else{
				if(args.length == 1 && args[0].equalsIgnoreCase("reload")){
					if(sender.hasPermission("SimpleReportsEx.reload") || sender.isOp()){
						config = new YamlConfiguration();
						try{
							config.load(configFile);
						}catch(FileNotFoundException e){
							e.printStackTrace();
						}catch(IOException e){
							e.printStackTrace();
						}catch(InvalidConfigurationException e){
							e.printStackTrace();
						}
						backend = config.getString("backend");
						if(backend.equalsIgnoreCase("file")){
							reports = new YamlConfiguration();
						}else if(backend.equalsIgnoreCase("sql")){
							sql = new SQL_sre(config.getString("sql.server"), config.getString("sql.user"), config.getString("sql.password"), config.getInt("sql.port"),
									config.getString("sql.database"), config.getString("sql.table-prefix"));
							sql.executeUpdate("CREATE TABLE IF NOT EXISTS `players` (`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto-generated ID of the player', `uuid` varchar(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'The UUID of the player', `last known name` varchar(32) NOT NULL COMMENT 'The last username that the player with this UUID used', PRIMARY KEY (id))");
							sql.executeUpdate("CREATE TABLE IF NOT EXISTS `reports` (`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto-generated ID of that report', `report` varchar(128) NOT NULL COMMENT 'The report-text', `reporter_id` int(11) NOT NULL COMMENT 'The player that created this report', `x` int(11) NOT NULL COMMENT 'The x-coordinate where the report was created', `y` int(11) NOT NULL COMMENT 'The y-coordinate where the report was created', `z` int(11) NOT NULL COMMENT 'The z-coordinate where the report was created', `timestamp` bigint(20) NOT NULL COMMENT 'The time when the report was created', `active` tinyint(1) NOT NULL COMMENT 'If the report is active or not', PRIMARY KEY (id))");
						}else{
							log(Level.SEVERE, "No valid input in config for backend! Using file");
							backend = "file";
						}
						loadYamls();
						lang.reloadLanguage(new File(getDataFolder(), "lang" + File.separator + config.getString("language") + ".yml"));
						sender.sendMessage("§aReload successful!");
					}else{
						sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.reload"));
						return true;
					}
				}else if(args.length == 1 && args[0].equalsIgnoreCase("purgeconfirm") && awaitingResponse){
					if(sender.hasPermission("SimpleReportsEx.purgereports") || sender.isOp()){
						new Thread(new Purger(sender)).start();
						awaitingResponse = false;
						return true;
					}else{
						sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.purgereports"));
						return true;
					}
				}else{
					try{
						sender.sendMessage("§l§eSimpleReportsEx Version " + this.getPluginLoader().getPluginDescription(getFile()).getVersion());
					}catch(InvalidDescriptionException e){
						e.printStackTrace();
					}
					sender.sendMessage("§8_____________________________________________________");
					sender.sendMessage("§9Help:");
					sender.sendMessage("§e/report [text]     §f- §7Report something");
					sender.sendMessage("§e/getreports        §f- §7Displays all unsolved reports");
					sender.sendMessage("§e/getreport [id]    §f- §7Displays detailed information about this report");
					sender.sendMessage("§e/tpreport [id]     §f- §7Teleports you to the place where this report was issued");
					sender.sendMessage("§e/solvereport [ID]  §f- §7Marks the specified report as solved");
					sender.sendMessage("§e/sre reload        §f- §7Reloads the config and the language-files");
					sender.sendMessage("§e/deletereport [ID] §f- §7Deletes the report with the given ID");
					sender.sendMessage("§e/purgereports      §f- §7Deletes all inactive reports");
				}
				return true;
			}
		}else if(cmd.getName().equalsIgnoreCase("report")){
			if(sender.hasPermission("SimpleReportsEx.report") || sender.isOp()){
				if(args.length < 1){
					if(args.length < 1){
						sender.sendMessage(lang.getMsg("few-args"));
					}
				}else{
					String reason = args[0];
					if(args.length > 1){
						for(int i = 1; i < args.length; i++){
							reason += " " + args[i];
						}
					}
					new Thread(new AddRepThread(reason, sender)).start();
					return true;
				}
			}else{
				sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.report"));
				return true;
			}
		}else if(cmd.getName().equalsIgnoreCase("getreports") || cmd.getName().equalsIgnoreCase("greports") || cmd.getName().equalsIgnoreCase("greps") || cmd.getName().equalsIgnoreCase("grep") || cmd.getName().equalsIgnoreCase("getreport")){
			if(sender.hasPermission("SimpleReportsEx.getreports") || sender.isOp()){
				if(args.length > 1){
					sender.sendMessage(lang.getMsg("many-args"));
				}else{
					if(args.length == 0){
						getReports(sender, true);
					}else{
						try{
							int id = Integer.parseInt(args[0]);
							getReports(sender, id);
						}catch(NumberFormatException e){
							sender.sendMessage(lang.getMsg("ID-Error"));
						}
					}
					return true;
				}
			}else{
				sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.getreports"));
				return true;
			}
		}else if(cmd.getName().equalsIgnoreCase("solvereport") || cmd.getName().equalsIgnoreCase("sreport") || cmd.getName().equalsIgnoreCase("srep")){
			if(sender.hasPermission("SimpleReportsEx.solvereport") || sender.isOp()){
				if(args.length != 1){
					if(args.length < 1){
						sender.sendMessage(lang.getMsg("few-args"));
					}else{
						sender.sendMessage(lang.getMsg("many-args"));
					}
				}else{
					int ID = 0;
					try{
						ID = Integer.parseInt(args[0]);
					}catch(NumberFormatException e){
						lang.getMsg("ID-Error");
						return true;
					}
					if(backend.equals("sql")){
						sql.executeUpdate("UPDATE reports SET `active` = '0' WHERE id = '" + ID + "'");
						sender.sendMessage(lang.getMsg("report-solved"));
					}else{
						if(reports.get("reports." + ID) != null){
							reports.set("reports." + ID + ".active", false);
							sender.sendMessage(lang.getMsg("report-solved"));
						}else{
							sender.sendMessage(lang.getMsg("report-not-existing"));
						}
					}
					return true;
				}
			}else{
				sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.solvereport"));
				return true;
			}
		}else if(cmd.getName().equalsIgnoreCase("tpreport") || cmd.getName().equalsIgnoreCase("tprep")){
			if(sender.hasPermission("SimpleReportsEx.tpreport") || sender.isOp()){
				if(args.length != 1){
					if(args.length < 1){
						sender.sendMessage(lang.getMsg("few-args"));
					}else{
						sender.sendMessage(lang.getMsg("many-args"));
					}
					return false;
				}else{
					if(sender instanceof Player){
						try{
							int ID = Integer.parseInt(args[0]);
							double x = 0, y = 0, z = 0;
							String world = "world";
							if(backend.equals("sql")){
								ResultSet res = sql.executeQuery("SELECT x,y,z,world FROM reports WHERE id = '" + ID + "'");
								try{
									while(res.next()){
										x = res.getInt("x");
										y = res.getInt("y");
										z = res.getInt("z");
										world = res.getString("world");
									}
								}catch(SQLException e){
									e.printStackTrace();
								}
							}else{
								if(reports.get("reports." + ID) != null){
									x = reports.getInt("reports." + ID + ".x");
									y = reports.getInt("reports." + ID + ".y");
									z = reports.getInt("reports." + ID + ".z");
									world = reports.getString("reports." + ID + ".world");
								}else{
									sender.sendMessage(lang.getMsg("no-report"));
								}
							}
							Player player = (Player) sender;
							Location tpdest = new Location(Bukkit.getServer().getWorld(world), x + 0.5, y, z + 0.5);
							// Noch Bearbeiten
							if(!Bukkit.getServer().getWorlds().contains(Bukkit.getServer().getWorld(world))){
								sender.sendMessage(lang.getMsg("no-world").replace("[WORLD]", world));
							}else{
								player.teleport(tpdest);
							}
						}catch(NumberFormatException e){
							sender.sendMessage(lang.getMsg("no-teleport"));
						}
					}else{
						sender.sendMessage(lang.getMsg("tpreport-player-only"));
					}
					return true;
				}
			}else{
				sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.tpreport"));
				return true;
			}
		}else if(cmd.getName().equalsIgnoreCase("purgereports")){
			if(sender.hasPermission("SimpleReportsEx.purgereports") || sender.isOp()){
				sender.sendMessage(lang.getMsg("purge-warn"));
				awaitingResponse = true;
				return true;
			}else{
				sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.purgereports"));
				return true;
			}
		}else if(cmd.getName().equalsIgnoreCase("deletereport") || cmd.getName().equalsIgnoreCase("delreport") || cmd.getName().equalsIgnoreCase("delrep")){
			if(sender.hasPermission("SimpleReportsEx.deletereport") || sender.isOp()){
				if(args.length != 1){
					if(args.length < 1){
						sender.sendMessage(lang.getMsg("few-args"));
					}else{
						sender.sendMessage(lang.getMsg("many-args"));
					}
					return false;
				}else{
					int ID = 0;
					try{
						ID = Integer.parseInt(args[0]);
					}catch(NumberFormatException e){
						sender.sendMessage(lang.getMsg("ID-Error"));
						return true;
					}
					delReport(sender, ID);
				}
				return true;
			}else{
				sender.sendMessage(lang.getMsg("no-perm").replace("[PERM]", "SimpleReportsEx.deletereport"));
				return true;
			}
		}
		return false;
	}

	public static void getReports(CommandSender sender, boolean sendIfNone){
		if(backend.equals("sql")){
			ResultSet res = sql.executeQuery("SELECT id FROM reports WHERE active = '1'");
			String result = "";
			try{
				if(res.next()){
					result = res.getString("id");
				}
				while(res.next()){
					result += ", " + res.getString("id");
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			if(result.equals("")){
				if(sendIfNone){
					sender.sendMessage(lang.getMsg("no-reports"));
				}
			}else{
				sender.sendMessage(lang.getMsg("getreports"));
				sender.sendMessage(result);
			}
		}else{
			List<String> keys = reports.getStringList("keys");
			List<String> openReps = new ArrayList<String>();
			for(int i = 0; i < keys.size(); i++){
				if(reports.getBoolean("reports." + keys.get(i) + ".active")){
					openReps.add(keys.get(i));
				}
			}
			String result = "";
			if(openReps.size() > 0){
				result = openReps.get(0);
				for(int i = 1; i < openReps.size(); i++){
					result += ", " + openReps.get(i);
				}
			}
			if(result.equals("")){
				if(sendIfNone){
					sender.sendMessage(lang.getMsg("no-reports"));
				}
			}else{
				sender.sendMessage(lang.getMsg("getreports"));
				sender.sendMessage(result);
			}
		}
	}

	public void getReports(CommandSender sender, int ID){
		new Thread(new GetRepThread(ID, sender)).start();
	}

	public void delReport(CommandSender sender, int ID){
		if(backend.equals("sql")){
			sql.executeUpdate("DELETE FROM reports WHERE id = '" + ID + "'");
			sender.sendMessage(lang.getMsg("report-deleted"));
		}else{
			if(reports.get("reports." + ID) != null){
				reports.set("reports." + ID, null);
				List<String> keys = reports.getStringList("keys");
				keys.remove(new Integer(ID).toString());
				reports.set("keys", keys);
				sender.sendMessage(lang.getMsg("report-deleted"));
			}else{
				sender.sendMessage(lang.getMsg("not-deleted"));
			}
		}
	}

	public static void delReport(int ID){
		if(backend.equals("sql")){
			sql.executeUpdate("DELETE FROM reports WHERE id = '" + ID + "'");
		}else{
			if(reports.get("reports." + ID) != null){
				reports.set("reports." + ID, null);
				List<String> keys = reports.getStringList("keys");
				keys.remove(new Integer(ID).toString());
				reports.set("keys", keys);
			}
		}
	}

	public static void log(String msg){
		logger.info("[SimpleReportsEx] " + msg);
	}

	public static void log(Level level, String msg){
		logger.log(level, "[SimpleReportsEx] " + msg);
	}

	public void loadYamls(){
		try{
			if(backend.equalsIgnoreCase("file")){
				reports.load(reportFile);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void saveYamls(){
		if(backend.equalsIgnoreCase("file")){
			try{
				reports.save(reportFile);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public static String getUUIDFromName(String name){
		UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(name));
		Map<String, UUID> response = null;
		try{
			response = fetcher.call();
		}catch(Exception e){
			log(Level.WARNING, "Exception while running UUIDFetcher!");
			e.printStackTrace();
		}
		if(response.get(name) == null){
			return null;
		}else{
			return response.get(name).toString();
		}
	}

	public static String getNameFromUUID(UUID uuid){
		NameFetcher fetcher = new NameFetcher(Arrays.asList(uuid));
		Map<UUID, String> response = null;
		try{
			response = fetcher.call();
		}catch(Exception e){
			log(Level.WARNING, "Exception while running NameFetcher!");
			e.printStackTrace();
		}
		if(response.get(uuid) == null){
			return null;
		}else{
			return response.get(uuid);
		}
	}
}